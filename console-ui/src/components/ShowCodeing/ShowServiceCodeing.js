/*
 * Copyright 1999-2018 radarfyh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React from 'react';
import PropTypes from 'prop-types';
import { getParams } from '../../globalLib';
import { ConfigProvider, Dialog, Loading, Tab } from '@alifd/next';

import './index.scss';

const TabPane = Tab.Item;

/**
 *
 * Service Registration Sample Code Show Window
 * @author yongchao9  #2019年05月18日 下午4:26:19
 *
 */
@ConfigProvider.config
class ShowServiceCodeing extends React.Component {
  static displayName = 'ShowServiceCodeing';

  static propTypes = {
    locale: PropTypes.object,
  };

  constructor(props) {
    super(props);
    this.state = {
      dialogvisible: false,
      loading: false,
    };
    this.defaultCode = '';
    this.nodejsCode = 'TODO';
    this.cppCode = 'TODO';
    this.shellCode = 'TODO';
    this.pythonCode = 'TODO';
    this.record = {};
    this.springCode = 'TODO';
    this.sprigbootCode = 'TODO';
    this.sprigcloudCode = 'TODO';
    this.csharpCode = 'TODO';
  }

  componentDidMount() {}

  openLoading() {
    this.setState({
      loading: true,
    });
  }

  closeLoading() {
    this.setState({
      loading: false,
    });
  }

  getData() {
    const namespace = getParams('namespace'); // 获取ak,sk
    const obj = {
      group: this.record.group || '',
      dataId: this.record.dataId || '',
      namespace,
      inEdas: window.globalConfig.isParentEdas(),
    };
    this.defaultCode = this.getJavaCode(obj);
    this.createCodeMirror('text/x-java', this.defaultCode);
    this.springCode = this.getSpringCode(obj);
    this.sprigbootCode = this.getSpringBootCode(obj);
    this.sprigcloudCode = this.getSpringCloudCode(obj);
    this.nodejsCode = this.getNodejsCode(obj);
    this.cppCode = this.getCppCode(obj);
    this.shellCode = this.getShellCode(obj);
    this.pythonCode = this.getPythonCode(obj);
    this.csharpCode = this.getCSharpCode(obj);
    this.forceUpdate();
  }

  getJavaCode(data) {
    return `/* Refer to document: https://github.com/alibaba/aip/blob/master/example/src/main/java/com/alibaba/aip/example
*  pom.xml
    <dependency>
        <groupId>work.metanet.aip</groupId>
        <artifactId>aip-client</artifactId>
        <version>$\{latest.version}</version>
    </dependency>
*/
package work.metanet.aip.example;

import java.util.Properties;

import work.metanet.aip.api.exception.AipException;
import work.metanet.aip.api.naming.NamingFactory;
import work.metanet.aip.api.naming.NamingService;
import work.metanet.aip.api.naming.listener.Event;
import work.metanet.aip.api.naming.listener.EventListener;
import work.metanet.aip.api.naming.listener.NamingEvent;

/**
 * @author nkorange
 */
public class NamingExample {

    public static void main(String[] args) throws AipException {

        Properties properties = new Properties();
        properties.setProperty("serverAddr", System.getProperty("serverAddr"));
        properties.setProperty("namespace", System.getProperty("namespace"));

        NamingService naming = NamingFactory.createNamingService(properties);

        naming.registerInstance("${this.record.name}", "11.11.11.11", 8888, "TEST1");

        naming.registerInstance("${this.record.name}", "2.2.2.2", 9999, "DEFAULT");

        System.out.println(naming.getAllInstances("${this.record.name}"));

        naming.deregisterInstance("${this.record.name}", "2.2.2.2", 9999, "DEFAULT");

        System.out.println(naming.getAllInstances("${this.record.name}"));

        naming.subscribe("${this.record.name}", new EventListener() {
            @Override
            public void onEvent(Event event) {
                System.out.println(((NamingEvent)event).getServiceName());
                System.out.println(((NamingEvent)event).getInstances());
            }
        });
    }
}`;
  }

  getSpringCode(data) {
    return `/* Refer to document: https://github.com/aip-group/aip-examples/tree/master/aip-spring-example/aip-spring-discovery-example
*  pom.xml
    <dependency>
        <groupId>work.metanet.aip</groupId>
        <artifactId>aip-spring-context</artifactId>
        <version>\${latest.version}</version>
    </dependency>
*/

// Refer to document:  https://github.com/aip-group/aip-examples/blob/master/aip-spring-example/aip-spring-discovery-example/src/main/java/com/alibaba/aip/example/spring
package work.metanet.aip.example.spring;

import work.metanet.aip.api.annotation.AipProperties;
import work.metanet.aip.spring.context.annotation.discovery.EnableAipDiscovery;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableAipDiscovery(globalProperties = @AipProperties(serverAddr = "127.0.0.1:8848"))
public class AipConfiguration {

}

// Refer to document: https://github.com/aip-group/aip-examples/tree/master/aip-spring-example/aip-spring-discovery-example/src/main/java/com/alibaba/aip/example/spring/controller
package work.metanet.aip.example.spring.controller;

import work.metanet.aip.api.annotation.AipInjected;
import work.metanet.aip.api.exception.AipException;
import work.metanet.aip.api.naming.NamingService;
import work.metanet.aip.api.naming.pojo.Instance;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@Controller
@RequestMapping("discovery")
public class DiscoveryController {

    @AipInjected
    private NamingService namingService;

    @RequestMapping(value = "/get", method = GET)
    @ResponseBody
    public List<Instance> get(@RequestParam String serviceName) throws AipException {
        return namingService.getAllInstances(serviceName);
    }
}`;
  }

  getSpringBootCode(data) {
    return `/* Refer to document: https://github.com/aip-group/aip-examples/blob/master/aip-spring-boot-example/aip-spring-boot-discovery-example
*  pom.xml
    <dependency>
       <groupId>com.alibaba.boot</groupId>
       <artifactId>aip-discovery-spring-boot-starter</artifactId>
       <version>\${latest.version}</version>
    </dependency>
*/
/* Refer to document:  https://github.com/aip-group/aip-examples/blob/master/aip-spring-boot-example/aip-spring-boot-discovery-example/src/main/resources
* application.properties
   aip.discovery.server-addr=127.0.0.1:8848
*/
// Refer to document: https://github.com/aip-group/aip-examples/blob/master/aip-spring-boot-example/aip-spring-boot-discovery-example/src/main/java/com/alibaba/aip/example/spring/boot/controller

package work.metanet.aip.example.spring.boot.controller;

import work.metanet.aip.api.annotation.AipInjected;
import work.metanet.aip.api.exception.AipException;
import work.metanet.aip.api.naming.NamingService;
import work.metanet.aip.api.naming.pojo.Instance;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@Controller
@RequestMapping("discovery")
public class DiscoveryController {

    @AipInjected
    private NamingService namingService;

    @RequestMapping(value = "/get", method = GET)
    @ResponseBody
    public List<Instance> get(@RequestParam String serviceName) throws AipException {
        return namingService.getAllInstances(serviceName);
    }
}`;
  }

  getSpringCloudCode(data) {
    return `/* Refer to document: https://github.com/aip-group/aip-examples/blob/master/aip-spring-cloud-example/aip-spring-cloud-discovery-example/
*  pom.xml
    <dependency>
       <groupId>org.springframework.cloud</groupId>
       <artifactId>spring-cloud-starter-alibaba-aip-discovery</artifactId>
       <version>\${latest.version}</version>
    </dependency>
*/

// aip-spring-cloud-provider-example

/* Refer to document:  https://github.com/aip-group/aip-examples/tree/master/aip-spring-cloud-example/aip-spring-cloud-discovery-example/aip-spring-cloud-provider-example/src/main/resources
* application.properties
server.port=18080
spring.application.name=${this.record.name}
spring.cloud.aip.discovery.server-addr=127.0.0.1:8848
*/

// Refer to document: https://github.com/aip-group/aip-examples/tree/master/aip-spring-cloud-example/aip-spring-cloud-discovery-example/aip-spring-cloud-provider-example/src/main/java/com/alibaba/aip/example/spring/cloud
package work.metanet.aip.example.spring.cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author xiaojing
 */
@SpringBootApplication
@EnableDiscoveryClient
public class AipProviderApplication {

  public static void main(String[] args) {
    SpringApplication.run(AipProviderApplication.class, args);
}

  @RestController
  class EchoController {
    @RequestMapping(value = "/echo/{string}", method = RequestMethod.GET)
    public String echo(@PathVariable String string) {
      return "Hello Aip Discovery " + string;
    }
  }
}

// aip-spring-cloud-consumer-example

/* Refer to document: https://github.com/aip-group/aip-examples/tree/master/aip-spring-cloud-example/aip-spring-cloud-discovery-example/aip-spring-cloud-consumer-example/src/main/resources
* application.properties
spring.application.name=micro-service-oauth2
spring.cloud.aip.discovery.server-addr=127.0.0.1:8848
*/

// Refer to document: https://github.com/aip-group/aip-examples/tree/master/aip-spring-cloud-example/aip-spring-cloud-discovery-example/aip-spring-cloud-consumer-example/src/main/java/com/alibaba/aip/example/spring/cloud
package work.metanet.aip.example.spring.cloud;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * @author xiaojing
 */
@SpringBootApplication
@EnableDiscoveryClient
public class AipConsumerApplication {

    @LoadBalanced
    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    public static void main(String[] args) {
        SpringApplication.run(AipConsumerApplication.class, args);
    }

    @RestController
    public class TestController {

        private final RestTemplate restTemplate;

        @Autowired
        public TestController(RestTemplate restTemplate) {this.restTemplate = restTemplate;}

        @RequestMapping(value = "/echo/{str}", method = RequestMethod.GET)
        public String echo(@PathVariable String str) {
            return restTemplate.getForObject("http://service-provider/echo/" + str, String.class);
        }
    }
}`;
  }

  getNodejsCode(data) {
    return 'TODO';
  }

  getCppCode(data) {
    return 'TODO';
  }

  getShellCode(data) {
    return 'TODO';
  }

  getPythonCode(data) {
    return 'TODO';
  }

  getCSharpCode(data) {
    return `/* Refer to document: https://github.com/aip-group/aip-sdk-csharp/
Demo for Basic Aip Opreation
App.csproj

<ItemGroup>
  <PackageReference Include="aip-sdk-csharp" Version="\${latest.version}" />
</ItemGroup>
*/

using Microsoft.Extensions.DependencyInjection;
using Aip.V2;
using Aip.V2.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

class Program
{
    static async Task Main(string[] args)
    {
        IServiceCollection services = new ServiceCollection();

        services.AddAipV2Naming(x =>
        {
            x.ServerAddresses = new List<string> { "http://localhost:8848/" };
            x.Namespace = "cs-test";

            // swich to use http or rpc
            x.NamingUseRpc = true;
        });

        IServiceProvider serviceProvider = services.BuildServiceProvider();
        var namingSvc = serviceProvider.GetService<IAipNamingService>();

        await namingSvc.RegisterInstance("${this.record.name}", "11.11.11.11", 8888, "TEST1");

        await namingSvc.RegisterInstance("${this.record.name}", "2.2.2.2", 9999, "DEFAULT");

        Console.WriteLine(Newtonsoft.Json.JsonConvert.SerializeObject(await namingSvc.GetAllInstances("${this.record.name}")));

        await namingSvc.DeregisterInstance("${this.record.name}", "2.2.2.2", 9999, "DEFAULT");

        var listener = new EventListener();

        await namingSvc.Subscribe("${this.record.name}", listener);
    }

    internal class EventListener : IEventListener
    {
        public Task OnEvent(IEvent @event)
        {
            Console.WriteLine(Newtonsoft.Json.JsonConvert.SerializeObject(@event));
            return Task.CompletedTask;
        }
    }
}

/* Refer to document: https://github.com/aip-group/aip-sdk-csharp/
Demo for ASP.NET Core Integration
App.csproj

<ItemGroup>
  <PackageReference Include="aip-sdk-csharp.AspNetCore" Version="\${latest.version}" />
</ItemGroup>
*/

/* Refer to document: https://github.com/aip-group/aip-sdk-csharp/blob/dev/samples/App1/appsettings.json
*  appsettings.json
{
  "aip": {
    "ServerAddresses": [ "http://localhost:8848" ],
    "DefaultTimeOut": 15000,
    "Namespace": "cs",
    "ServiceName": "App1",
    "GroupName": "DEFAULT_GROUP",
    "ClusterName": "DEFAULT",
    "Port": 0,
    "Weight": 100,
    "RegisterEnabled": true,
    "InstanceEnabled": true,
    "Ephemeral": true,
    "NamingUseRpc": true,
    "NamingLoadCacheAtStart": ""
  }
}
*/

// Refer to document: https://github.com/aip-group/aip-sdk-csharp/blob/dev/samples/App1/Startup.cs
using Aip.AspNetCore.V2;

public class Startup
{
    public Startup(IConfiguration configuration)
    {
        Configuration = configuration;
    }

    public IConfiguration Configuration { get; }

    public void ConfigureServices(IServiceCollection services)
    {
        // ....
        services.AddAipAspNet(Configuration);
    }

    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
        // ....
    }
}
    `;
  }

  openDialog(record) {
    this.setState({
      dialogvisible: true,
    });
    this.record = record;
    setTimeout(() => {
      this.getData(); // 获取数据
    });
  }

  closeDialog() {
    this.setState({
      dialogvisible: false,
    });
  }

  createCodeMirror(mode, value) {
    const commontarget = this.refs.codepreview;
    if (commontarget) {
      commontarget.innerHTML = '';
      this.cm = window.CodeMirror(commontarget, {
        value,
        mode,
        height: 400,
        width: 500,
        lineNumbers: true,
        theme: 'xq-light',
        lint: true,
        tabMode: 'indent',
        autoMatchParens: true,
        textWrapping: true,
        gutters: ['CodeMirror-lint-markers'],
        extraKeys: {
          F1(cm) {
            cm.setOption('fullScreen', !cm.getOption('fullScreen'));
          },
          Esc(cm) {
            if (cm.getOption('fullScreen')) cm.setOption('fullScreen', false);
          },
        },
      });
      // this.cm.setSize('300px',height:'222px');
      this.cm.setSize('auto', '490px');
    }

    // this.cm.setSize(window.innerWidth*0.8-10,400);//设置宽高
  }

  changeTab(key, code) {
    setTimeout(() => {
      this[key] = true;

      this.createCodeMirror('text/javascript', code);
    });
  }

  render() {
    const { locale = {} } = this.props;
    const footer = <div />;
    return (
      <div>
        <Dialog
          title={locale.sampleCode}
          style={{ width: '80%' }}
          visible={this.state.dialogvisible}
          footer={footer}
          onClose={this.closeDialog.bind(this)}
        >
          <div style={{ height: 500 }}>
            <Loading tip={locale.loading} style={{ width: '100%' }} visible={this.state.loading}>
              <Tab shape={'text'} style={{ height: 40, paddingBottom: 10 }}>
                <TabPane
                  title={'Java'}
                  key={0}
                  onClick={this.changeTab.bind(this, 'commoneditor1', this.defaultCode)}
                />
                <TabPane
                  title={'Spring'}
                  key={1}
                  onClick={this.changeTab.bind(this, 'commoneditor1', this.springCode)}
                />
                <TabPane
                  title={'Spring Boot'}
                  key={2}
                  onClick={this.changeTab.bind(this, 'commoneditor2', this.sprigbootCode)}
                />

                <TabPane
                  title={'Spring Cloud'}
                  key={21}
                  onClick={this.changeTab.bind(this, 'commoneditor21', this.sprigcloudCode)}
                />

                <TabPane
                  title={'Node.js'}
                  key={3}
                  onClick={this.changeTab.bind(this, 'commoneditor3', this.nodejsCode)}
                />

                <TabPane
                  title={'C++'}
                  key={4}
                  onClick={this.changeTab.bind(this, 'commoneditor4', this.cppCode)}
                />

                <TabPane
                  title={'Shell'}
                  key={5}
                  onClick={this.changeTab.bind(this, 'commoneditor5', this.shellCode)}
                />

                <TabPane
                  title={'Python'}
                  key={6}
                  onClick={this.changeTab.bind(this, 'commoneditor6', this.pythonCode)}
                />

                <TabPane
                  title={'C#'}
                  key={7}
                  onClick={this.changeTab.bind(this, 'commoneditor7', this.csharpCode)}
                />
                {}
              </Tab>
              <div ref={'codepreview'} />
            </Loading>
          </div>
        </Dialog>
      </div>
    );
  }
}

export default ShowServiceCodeing;
